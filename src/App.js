import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect } from 'react';
import reactDom from 'react-dom';
import ProductView from './components/ProductView';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';
import TextField from '@mui/material/TextField';
import Slider from '@mui/material/Slider';
import Paper from '@mui/material/Paper';
import TablePagination from '@mui/material/TablePagination';
import Grid from '@mui/material/Grid';

function App() {
  const [items, setItems] = useState([]);
  const [error, setError] = useState();
  const [checked, setChecked] = React.useState(true);
  const [search, setSearch] = React.useState('');
  const [value, setValue] = React.useState(200);
  const [page, setPage] = React.useState(1);
  const [limit, setLimit] = React.useState(10);

  useEffect(() => {
    callItem();
  }, []);
  const callItem = () => {
    fetch(`http://localhost:3010/products?_page=${page}&_limit=${limit}`)
      .then((res) => res.json())
      .then(
        (result) => {
          setItems(result);
        },
        (error) => {
          setError(error);
        },
      );
  };

  const handleCheckedFunction = (event) => {
    setChecked(event.target.checked);
  };
  const handlePriceSliderFunction = (event, newValue) => {
    if (typeof newValue === 'number') {
      setValue(newValue);
    }
  };
  const handleSearchFunction = (event) => {
    setSearch(event.target.value);
  };
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    console.log('page ' + newPage);
    callItem();
  };

  const handleChangeRowsPerPage = (event) => {
    setLimit(parseInt(event.target.value, 10));
    setPage(0);
    console.log('limit ' + limit);
    callItem();
  };
  return (
    <Container minWidth="sm" className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Typography gutterBottom>Product Listing</Typography>
      </header>
      <Box className="fullPage">
        <Box className="sideBar">
          <Typography variant="button" display="block" gutterBottom>
            Filters
          </Typography>
          <TextField
            id="outlined-basic"
            label="Search"
            variant="outlined"
            value={search}
            onChange={handleSearchFunction}
          />
          <Typography
            id="non-linear-slider"
            variant="button"
            display="block"
            gutterBottom
          >
            Price
          </Typography>

          <Slider
            value={value}
            min={0}
            step={5}
            max={200}
            onChange={handlePriceSliderFunction}
            valueLabelDisplay="auto"
            aria-labelledby="non-linear-slider"
          />

          {/* <FormControlLabel
            label="Subscription"
            control={
              <Checkbox checked={checked} onChange={handleCheckedFunction} />
            }
          /> */}
        </Box>
        <Box className="productList">
          <Grid container spacing={2}>
            {items
              .filter((item) => item.price < value)
              .map((item) => {
                if (item.tags.includes(search)) {
                  return <ProductView product={item} />;
                } else if (search === '') {
                  return <ProductView product={item} />;
                }
              })}
          </Grid>
          <TablePagination
            component="div"
            count={100}
            page={page}
            onPageChange={handleChangePage}
            rowsPerPage={limit}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Box>
      </Box>
      <footer className="App-header">
        <Typography gutterBottom>@ 2022 - PetLab Co.</Typography>
      </footer>
    </Container>
  );
}

export default App;
