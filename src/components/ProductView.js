import React from 'react';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';
import TextField from '@mui/material/TextField';
import Slider from '@mui/material/Slider';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import Style from './Style.css';

const ProductView = ({ product }) => {
  return (
    <Grid item xs={4}>
      <Link href={product.url} target="_blank" underline="none" color="inherit">
        <Box key={product.id} className="GridView">
          <img src={product.image_src} alt={product.title} width={200} />
          <Typography variant="h6" gutterBottom component="div">
            {product.title}
          </Typography>
          <Typography gutterBottom>GBP {product.price}</Typography>
        </Box>
      </Link>
    </Grid>
  );
};

export default ProductView;
