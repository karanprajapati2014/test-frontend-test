import { render, screen } from '@testing-library/react';
import App from './App';

describe('<App />', () => {
  test('Test 1 - 12 Products ', () => {
    const items = fetch('http://localhost:3010/products').then((res) =>
      res.json(),
    );
    // - Collection of ProductList
    // - Check Sidebar ID is there or Not
    // - Display with the pagination and expected 12 products
    expect(items).toBe(items);
  });

  test('Test 2 - Search By Dog', () => {
    const items = fetch('http://localhost:3010/products/?tags_like=Dog').then(
      (res) => res.json(),
    );

    // - Collection of ProductList
    // - Check Sidebar ID is there or Not
    // - search 'Dog' key work and compare the Data
    // - Display with the pagination and expected 10 products
    expect(items).toBe(items);
  });

  test('Test 3 - Price 30', () => {
    const items = fetch('http://localhost:3010/products/').then((res) =>
      res.json(),
    );

    // - Collection of ProductList
    // - Check Sidebar ID is there or Not
    // - search price - 30  key work and compare the Data
    // - Display with the pagination and expected 1 products
    expect(items).toBe(items);
  });
});
